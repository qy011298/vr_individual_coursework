## Preamble:

```
Module Code:   CS3VR16
Assignment report Title:     VR Individual World
Student Number: 26011298
Date (when the work completed): TBD
Actual hrs spent for the assignment: Must be on like... 20-25 now?
Assignment evaluation (3 key points):
	- Interesting
	- Difficult, given the lack of support on unity
	- Creative
```

# Virtual Reality Coursework 1

## Brief Overview:

The virtual world I have created is meant to portray a scene, in which the player is acting as a person who lives on an off-earth planet. As such, the world themes are futuristic and spacey.(**)

The house was modelled in Unity using ProBuilder (), a tool that can be added to Unity. The house was modelled using a variety of 'pipes', 'cubes' and 'planes'. The textures on both the interior and the exterior are meant to reflect the overall theme, and as such, are fairly neutral coloured, metallic materials, lit with white light. The ceiling was produced by placing two opposite facing planes together, giving them the same glass texture and making them transparent. This produces a ceiling that looks like glass from both 

  All non-furniture aspects of the house were built myself, including the overall sturcture, the button the player is able to interact with, and the sign referring to it. The furniture within the house are primarily imported assets from the unity store, that fit the overall theme of the house (). 

  In keeping with the futuristic theme, the player is able to interact with the door by pressing 'e', after which, the door will slide open. Upon entering the main room, a voice will say "Good evening, User ID 26011298", which is the incorporation of my Student ID number into the world.

  In the "Bedroom" of the house, is an button that was added to give some additional ways the player can interact with the world. When pressing the button, several animations are triggered that will simultaneously run - the pushing of the button, the movement/rotation of another planet overhead, and the rotation of the directional light that acts as the Sun, giving the impression that it has become night.

** Pictures of Button & furniture **

The exterior of the house was made using Unity's in-build terrain generator. Several tutorials () were followed that explained how to use the terrain tools. In addition, Unity's "Terrain Tools Sample Assets Pack"() was used to give additioanl brushes and textures, to produce the mountainous / rocky terrain the encompasses the building.

  To reflect the idea that the player "lives" on this planet, a small spaceship prefab was imported () from the Unity Asset Store, that fit the theme of the house and planet overall. 

  Due to the terrain being largely mountainous, to add some realism to the world, a cave was built and added to the world. This was done using probuilder and a tutorial (**)(). Probuilder allows the user to manipulate the basic shapes by their verticies, faces or edges, which was the process used to manipulate the shapes to give a realistic cave look. After this, textures and rock prefabs were imported from the Unity Asset Store () that were added to the scene to mold the cave into the terrain convincingly.  

** Insert 2 pictures showing the house and the cave **
** Insert 2 Pictures looking out of the cave in daytime and in nighttime ** 

Several scripts were written to control the user interactable aspects of the world, specifically the script to control the building's door (), and the script to control the animations that occurred when pressing the button (). 
These scripts use triggers to control animation controllers, as well as other things such as the UI "Press 'e' to interact" messages, and the audio when entering the building. 

  Animation controllers are the framework in which animations are run. They act like finite state machines, in that parameters can be added that are then used to determine the state of the animation. For example, the script that controls the door uses a boolean parameter called 'open' in the Animator Controller. That causes the state to go from 'doNothing' to 'openDoor', and triggering the animation.

  In the case of the door script, Audio Source / Audio listeners were used to emit a sound when either the door was opened (the door opening sound), or the character left the trigger area (the robotic voice saying the Student ID number).

  The character movement was implemented by importing the FPSController as part of the Unity Standard Assets package. The controller gave the basis for the movement of a First Person character, as well as giving enough customisation to alter the movement to give the impression that the character was in a low gravity setting.

## Controls:

 - WASD / Arrow Keys to move
 - E to interact when prompted
 - Left Shift to 'sprint'
 - Space to jump
 - Look around with the mouse pointer

## Reflection:

I felt that several aspects of the project went very well. For instance, once Probuilder had been learned, it became fairly easy to create interesting and convincing assets that went with the world - a prime example being the cave in the terrain. In addition, it allowed for modification of the individual faces on each object, making it very easy to apply different textures and materials to create objects such as Windows.

I think the use of animations allowed me to add a lot more depth to the world, and implemented aspects that added to the theme. For example, animations enabled me to create a futuristic style door to the house, that slides open and shut. I found the animation controls to be very intuiative and so it was straight forward to produce interesting and professional looking movements of objects, in the world.
  
On the other hand, some aspects of the world could be improved. I think that the terrain ended up being sparse and uninteresting. Despite this, I do think that it is in keeping with the theme of the world, however I think I should have decided on a different theme that would have allowed more creativity in the terrain and general world.

If I were to do this project again, I would want to create more assets for the world, in an alternative software such as Blender, as opposed to just importing a lot of them from the Unity Store. A change in the overall theme of the world, along with personally created assets, I think would produce a more immersive experience in the world.

Overall, the use of Unity in the project resulted in a good virtual world. The endless resources to learn Unity and the free assets on the store, make it very accessible, if a bit daunting to start with. As a result, Unity is able to produce a good product, with enough patience to learn the idiosyncracies of it.

## References:

### Websites / Links:

- Probuilder

### Tutorials:

- Terrain Tools: https://www.youtube.com/watch?v=2Vvwjfp-hg8
- Jayanam Youtube Videos on Animating door
- Probuilder tutorial for Cave

### Assets:

- Standard Assets: 
- Terrain Tools Sample Asset Pack: 
	
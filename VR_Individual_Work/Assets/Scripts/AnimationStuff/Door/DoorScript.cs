﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{

    [SerializeField] private AudioClip m_DoorOpenSound;
    [SerializeField] private AudioClip m_DoorCloseSound;
    

    Animator _animator;
    public GameObject OpenText = null;
    private AudioSource m_AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        _animator = transform.Find("FrontDoor").GetComponent<Animator>();
        OpenText.SetActive(false);
        m_AudioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other){
        if (other.tag == "Player"){
            OpenText.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other){
        if (other.tag == "Player"){
            _animator.SetBool("open",false);
            OpenText.SetActive(false);
            PlayDoorSound(2);
        }
    }

    private bool isOpenTextActive{
        get
        {
            return OpenText.activeSelf;
        }
    }

    private void PlayDoorSound(int mode){
        if (mode == 1){
            m_AudioSource.clip = m_DoorOpenSound;
        } else{
            m_AudioSource.clip = m_DoorCloseSound;
        }
        
        m_AudioSource.Play();
    }

        // Update is called once per frame
    void Update()
    {
        if(isOpenTextActive){
            if(Input.GetKeyDown(KeyCode.E)){
                OpenText.SetActive(false);
                _animator.SetBool("open", true);
                PlayDoorSound(1);
            }
        }
        
    }
}

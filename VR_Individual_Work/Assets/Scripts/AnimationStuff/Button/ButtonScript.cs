﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{

    private Animator btn_animator;
    private Animator planet_animator;
    private Animator light_animator;

    public GameObject OpenText = null;
    public int ButtonTimer = 100;
    // Start is called before the first frame update
    void Start()
    {
        btn_animator = transform.Find("UpperButton").GetComponent<Animator>();
        planet_animator = transform.Find("Planet").GetComponent<Animator>();
        light_animator = transform.Find("Directional Light").GetComponent<Animator>();
        OpenText.SetActive(false);
    }

    void OnTriggerEnter(Collider other){
        if (other.tag == "Player"){
            OpenText.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other){
        if (other.tag == "Player"){
            OpenText.SetActive(false);
        }
    }

    private bool isOpenTextActive{
        get
        {
            return OpenText.activeSelf;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isOpenTextActive){
            if(Input.GetKeyDown(KeyCode.E)){
                OpenText.SetActive(false);
                btn_animator.SetInteger("pressCounter",0);
                btn_animator.SetBool("pressed", true);
                if (light_animator.GetBool("pressed")){
                    light_animator.SetBool("rtnTime",true);
                    light_animator.SetBool("pressed",false);
                }
                else{
                    light_animator.SetBool("pressed",true);
                    light_animator.SetBool("rtnTime",false);
                }
                if (planet_animator.GetBool("pressed")){
                    planet_animator.SetBool("rtnPlanet",true);
                    planet_animator.SetBool("pressed",false);
                }
                else{
                    planet_animator.SetBool("pressed",true);
                    planet_animator.SetBool("rtnPlanet",false);
                }
            }
        }
        if (btn_animator.GetBool("pressed")){
            btn_animator.SetInteger("pressCounter",btn_animator.GetInteger("pressCounter")+1);
        }
        if (btn_animator.GetInteger("pressCounter") > ButtonTimer){
            btn_animator.SetBool("pressed", false);
        }
    }
}
